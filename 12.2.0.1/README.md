# Version 12.2.0.1

## Pull from dockerhub

```
$ docker pull mjstealey/oracle-instantclient:12.2.0.1
```

## Build from source

### Required files

Download the Oracle Instant Client RPMs from OTN (Account required to download):

[http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html](http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html)

The following three RPMs are required:

- `oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm`
- `oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm`
- `oracle-instantclient12.2-sqlplus-12.2.0.1.0-1.x86_64.rpm`


### Building

Place the downloaded Oracle Instant Client RPMs (from the previous step) in the
same directory as the `Dockerfile` and run:

```
docker build -t oracle-instantclient:12.2.0.1 .
```

