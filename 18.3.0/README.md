# Version 18.3.0

## Pull from dockerhub

```
$ docker pull mjstealey/oracle-instantclient:18.3.0
```

## Build from source

### Required files

Download the Oracle Instant Client RPMs from OTN (Account required to download):

[http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html](http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html)

The following three RPMs are required:

- `oracle-instantclient18.3-basic-18.3.0.0.0-1.x86_64.rpm`
- `oracle-instantclient18.3-devel-18.3.0.0.0-1.x86_64.rpm`
- `oracle-instantclient18.3-sqlplus-18.3.0.0.0-1.x86_64.rpm`


### Building

Place the downloaded Oracle Instant Client RPMs (from the previous step) in the
same directory as the `Dockerfile` and run:

```
docker build -t oracle-instantclient:18.3.0 .
```

